# Cars API

Repository containing backend code of the `cars` REST APIs - to store and retrieve the make and model of cars.  


### Installation

First, clone the repository.  

```bash
git clone https://jishinjayarajan@bitbucket.org/jishinjayarajan/django_cars.git
cd django_cars
```
To install all the library dependencies, run
```python
pip install -r requirements.txt
```

### Usage

To run the server, execute

```bash
python manage.py runserver
```
This runs the webserver at `127.0.0.1:8000`
 
### Request and Response Structure

A `GET` request to /cars/ would return 

**Response**
```json
{
    "id": 4,
    "make": "BMW",
    "model": "3 series"
}
```

For `POST` requests - 

**Sample Request**

```json
{
    "make": "Mercedes",
    "model": "S class"
}
```
This would insert the above fields into the DB. 

The sample POSTMAN requests collection is available in the media folder. 